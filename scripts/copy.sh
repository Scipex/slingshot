#!/bin/bash

udisksctl mount -b /dev/sdb1
rm /run/media/scipex/boot/kernel8.img
cp target/aarch64-unknown-none-softfloat/release/kernel8.img /run/media/scipex/boot/
echo "Copied Kernel image"
udisksctl unmount -b /dev/sdb1