#![allow(dead_code)]

use core::ptr::{read_volatile, write_volatile};

const GPIO_BASE: u32 = 0xFE200000;

fn write_gpio(value: u32, reg: u32, off: u32, bits: u32) {
    unsafe {
        let mut current = read_volatile(reg as *const u32);
        let bitmask: u32 = !((1 << (bits + 1) - 1) << off);
        current &= bitmask;
        current |= value << off;
        write_volatile(reg as *mut u32, current);
    }
}

#[allow(dead_code)]
pub enum Mode {
    Input = 0b000,
    Output = 0b001,
    Alt0 = 0b100,
    Alt1 = 0b101,
    Alt2 = 0b110,
    Alt3 = 0b111,
    Alt4 = 0b011,
    Alt5 = 0b010,
}
pub fn set_mode(pin: u32, mode: Mode) {
    // if pin > 57 {
    //     return;
    // }

    write_gpio(mode as u32, GPIO_BASE + (pin / 10) * 4, (pin % 10) * 3, 3);
}

#[allow(dead_code)]
pub enum Value {
    High,
    Low,
}
pub fn set_value(pin: u32, value: Value) {
    // if pin > 57 {
    // return;
    // }

    let reg = match value {
        Value::High => GPIO_BASE + 0x1C,
        Value::Low => GPIO_BASE + 0x28,
    };

    write_gpio(1, reg + (pin / 32) * 4, pin % 32, 1);
}
