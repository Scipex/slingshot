pub trait Driver {
    fn name(&self) -> &'static str;

    unsafe fn init(&self);
    fn after_init(&self);
}

pub struct DriverManager {
    drivers: [&'static dyn Driver; 0],
}

impl DriverManager {
    pub fn new() -> DriverManager {
        DriverManager { drivers: [] }
    }

    pub fn init(&self) {
        for driver in self.drivers {
            unsafe {
                driver.init();
            }
        }

        for driver in self.drivers {
            driver.after_init();
        }
    }
}
