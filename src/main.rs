#![no_std]
#![no_main]
#![feature(global_asm)]

mod boot;
mod driver;
mod gpio;

use driver::DriverManager;

fn kernel_init() -> ! {
    gpio::set_mode(23, gpio::Mode::Output);

    let driver_manager = DriverManager::new();
    driver_manager.init();

    gpio::set_value(23, gpio::Value::High);

    loop {}
}

// Panic handler
use core::panic::PanicInfo;

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    unimplemented!()
}
